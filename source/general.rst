.. _general:

Allgemeine Vorgaben
===================

Hierbei handelt sich um generelle Informationen, die noch nichts mit dem
eigentlichen netCDF-Format zu tun haben, aber bei der Erstellung von
Dateien berücksichtigt werden sollen.

Dateinamen
----------

- NetCDF-Dateien haben die *extension* .nc

- Der Dateiname beginnt mit einem Buchstaben oder einer
  Buchstabensequenz. Die Buchstabensequenz soll Rückschlüsse auf die
  Art der in der Datei befindlichen Daten zulassen.

- Bisher eingebürgerte Sequenzen (z.B. "ctd" für CTD-Daten, "sf" für
  ScanFish-Daten) werden beibehalten. Für andere Geräte sollte eine
  Festlegung noch erfolgen.

- Im Falle von Modelldaten sollte entweder der Modellname mit Version
  oder die ExperimentID im ersten Teil des Dateinamens verwendet
  werden.

- Leerzeichen, Umlaute, ß, Sonderzeichen (außer Unterstrich "_",
  Bindestrich "-" und Punkt) im Dateinamen sind verboten.

- Im Dateinamen werden Beginn (und evtl. das Ende) der in der Datei
  befindlichen Daten im Format YYYYMMDDHH kodiert. Dabei richtet sich
  das angegebene Datum nach der Gültigkeit der Daten. Eine Jahresdatei
  enthält in der Regel nur die Jahreszahl als YYYY. Falls sinnvoll,
  können die Minuten im Format mm und die Sekunden im Format SS
  angehängt werden. Nicht aufgelöste Stellen können weggelassen werden.
  An das Datum können weitere Informationen nach "_" oder "." angehängt
  werden, z.B. Variablen-Bezeichnung, Region, Kürzel für statistische
  Bearbeitung.


.. card:: Beispiele

   - ``ctd200904171324.nc`` für ein CTD-Profil, das am 17. April 2009 um 13 Uhr
     24 begonnen wurde.
   - ``WAVE2016080312_gb.nc`` als Output einer Wellenmodellrechnung für den
     03.08.2016 12:00 in der Deutschen Bucht (gb).
   - ``cD3_0025_ERAi.1948-2015.T_2M.DB.mm.nc`` als Zeitserie aus dem Output einer
     COSMO-CLM Simulation für die 2m-Lufttemperatur reduziert auf die
     Deutschen Bucht und *monthly mean*.

Namenskonventionen
------------------

- *Variables*, *dimensions* und *attribute names* beginnen mit einem
  Buchstaben und werden aus Buchstaben, Ziffern und Unterstrich
  zusammengesetzt.

- Leerzeichen, Umlaute, ß, Sonderzeichen (außer Unterstrich "_") sind
  verboten.

- Groß- und Kleinschreibung sind relevant. Vergleiche Beispiel 3.

*Names* dürfen sich nicht allein durch die Groß-/Kleinschreibung
unterscheiden.

*Variable names* sollten sich so weit wie möglich an internationale
Standards halten. Für beobachtbare marine Größen ist das z.B. die
P09-Datenbank des BODC [6]_.

.. card:: Beispiele

   - pressure, longitude, DRYT, air_pressure, FlugHoehe sind gültige *names*.
     Von den aufgeführten Variablennamen ist nur DRYT (dry bulb temperature)
     in P09 enthalten.
   - geogr. Breite, air pressure, Flughöhe, $velocity sind ungültige *names*.
   - *Variable names* wie PRESSURE und pressure dürfen nicht zusammen in
      derselben netCDF-Datei verwendet werden, da sie sich lediglich in der
      Groß-/Kleinschreibung unterscheiden. Gleiches gilt für *dimensions* und
      *attribute names*.

.. [6] http://vocab.nerc.ac.uk/collection/P09/current/
