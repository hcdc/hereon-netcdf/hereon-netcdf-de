.. _global-attributes:

Angaben zum gesamten Datensatz (globale Attribute)
==================================================

In den globalen Attributen werden beschreibende Informationen abgelegt,
die den gesamten Datensatz betreffen.

Es wird zwischen verpflichtenden Attributen (Kapitel 3.1) und optionalen
(Kapitel 3.2) unterschieden. Die optionalen sind notwendig, um den
unterschiedlichen Datenquellen/-disziplinen gerecht zu werden.

*Attribute names* sind grundsätzlich frei wählbar, es gibt allerdings
bereits reservierte *attribute names*, die nur für definierte Zwecke
verwendet werden dürfen. Diese sind::

    add_offset, ancillary_variables, axis, bounds, calendar, cell_measures,
    cell_methods, climatology, comment, compress, Conventions, coordinates,
    _FillValue, flag_masks, flag_meanings, flag_values, formula_terms,
    grid_mapping, history, institution, leap_month, leap_year, long_name,
    missing_value, month_lengths, positive, references, scale_factor,
    source, standard_error_multiplier, standard_name, title, units,
    valid_max, valid_min, valid_range.

Zur Bedeutung siehe Appendix A in *NetCDF Climate and Forecast (CF)
Metadata Conventions*\ [7]_.


.. _global-mandatory:

Verpflichtende globale Attribute
--------------------------------

*Global attributes* sind die Metadaten der netCDF-Datei. Darin dürfen
nur Informationen stecken, die für die Datei als Ganzes gelten.

.. metadata_table:: data/global_mandatory.yml

.. [7] http://cfconventions.org/cf-conventions/cf-conventions.html#attribute-appendix


.. _global-optional:

Optionale globale Attribute
---------------------------

Optionale globale Attribute sind nicht für alle netCDF-Dateien am Hereon
relevant, können aber in einzelnen Gruppen als notwendig vereinbart
werden. Wenn das beschriebene Attribut relevant ist, dann sollte dieses
Attribut aber einheitlich Verwendung finden.

Insbesondere sollte man für die Beschreibung des Dateiinhaltes das
Attribut comment wählen.

carrier beschreibt den Träger auf dem eine Plattform temporär
installiert ist, also z.B. RV Prandtl.

In COSYNA ist das Attribut platform verpflichtend und in der Regel
vorgegeben. Ein Beispiel für platform wäre "Ferrybox on RV Polarstern".

lineage beschreibt die Abstammung einer netCDF-Datei. Dies ist in der
Regel eine ursprüngliche Datei eines anderen Formats.

Die einzelnen Prozessierungsschritte sollte man in processingSteps
benennen.

StartTime und StopTime bezeichnen den ersten und letzten Zeitwert und
fungieren als schneller und einfacher Zugang zu diesen Werten, z.B. über
OPeNDAP.

Im distribution_statement werden Rechte und Pflichten der Nutzer der
Datei benannt, für den Fall, dass die Datei nach außen weitergegeben
oder veröffentlicht wird. Der Inhalt sollte im jeweiligen Institut
abgestimmt sein.

In history kann man direkt niederlegen, was mit der Datei gemacht wurde,
um zum aktuellen Zustand zu kommen. Tools wie die CDOs oder NCOs
dokumentieren Arbeitsschritte in history automatisch. Man kann hier auch
auf eine externe Datei oder eine URL verweisen.

*PI* soll den verantwortlichen Wissenschaftler für das ganze Projekt
benennen, zu dem diese Datei gehört.

Für die Beschreibung der geographischen Lage stehen folgende Attribute
zur Verfügung: geospatial_lon_min/_max, geospatial_lat_min/_max oder
Bbox . Sie sind z.B. für Abfragen von Metadatenkatalogen notwendig.

.. metadata_table:: data/global_optional.yml

.. _global-dimensions:

Dimensionen (*dimensions*)
--------------------------

In netCDF-Files werden die Daten als mehrdimensionale Felder abgelegt.
Entscheidendes Strukturelement sind dabei die *dimensions.* Hierüber
wird die interne Koordinatenstruktur der Datensätze (*variables*)
definiert. Sie beinhaltet Angaben zur Ausdehnung (nx,ny,nz) sowie zur
Zeitdimension (time). Sie werden unter *variables* (s. Abschnitt 4.)
erläutert.

-  Eine *variable* kann beliebig viele *dimensions* haben.

-  Alle *dimensions* müssen verschiedene *names* haben.

-  Eine Beschränkung auf 4 *dimensions* wird empfohlen.

-  *Dimensions* einer Variablen sollten in der relativen Ordnung
   Datum/Zeit, Höhe/Tiefe, Breite, Länge (T,Z,Y,X) angeordnet sein. Wo
   dies nicht möglich ist (z.B. bei Spektren), sind andere sinnvolle
   Anordnungen zu entwerfen.

Zusätzliche *dimensions* müssen links von (T,Z,Y,X) stehen.

Wenn Zeitreihen in netCDF-Files gespeichert werden, dann muss
berücksichtigt werden, dass die Zeitreihe weitergeführt werden kann,
indem neue Zeiten hinzugefügt werden. Die Dimension der Zeitvariable
sollte "UNLIMITED" sein, um das Erweitern von Zeitreihen problemlos
sicherstellen zu können. Sie ist dann die sogenannte *record dimension*.
