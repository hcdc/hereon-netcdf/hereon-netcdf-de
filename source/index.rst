==============================================================
Verbindliche Regeln für die Datenspeicherung in netCDF-Dateien
==============================================================


.. only:: html

      .. authorlist::
            :all:
            :affiliations:


.. toctree::
   :maxdepth: 2
   :caption: Inhalt

   intro
   general
   global_attributes
   variable_attributes


.. toctree::
   :maxdepth: 2
   :caption: Appendix

   coordinate_examples
   summary_tables


Zitieranweisungen
-----------------

.. card:: Bitte zitieren Sie diese Dokumentation!

   .. tab-set::

      .. tab-item:: APA

         .. citation-info::
            :format: apalike

      .. tab-item:: BibTex

         .. citation-info::
            :format: bibtex

      .. tab-item:: RIS

         .. citation-info::
            :format: ris

      .. tab-item:: Endnote

         .. citation-info::
            :format: endnote

      .. tab-item:: CFF

         .. citation-info::
            :format: cff



Indizes und Tabellen
====================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
