.. _intro:

Einführung
==========

Am Hereon werden in verschiedenen Instituten netCDF-Dateien geschrieben
und genutzt. Daher sollten die aus dem Zentrum stammenden netCDF-Dateien
in einer einheitlichen Nomenklatur geschrieben werden. Dies fördert und
erleichtert den Austausch und die Verbreitung. Sowohl für Attribute als
auch Variablen gibt es Vorgaben bezüglich der Namensgebung
(Namenskonventionen). Zudem finden sich Vorgaben, welche Attribute
bestimmten Variablen beigefügt werden sollten. Nicht alle Vorgaben sind
zwingend, und in bestimmten Fällen sind auch Anpassungen notwendig.

Im Wesentlichen wird sich dabei auf die in den *netCDF Climate and
Forecast (CF) Metadata Conventions*\  [1]_ (im Folgenden *CF
Conventions* genannt) getroffenen Vereinbarungen bezogen. Im Folgenden
werden die relevanten verbindlichen Regeln zusammengefasst und ergänzt.

Neben den CF Conventions gibt es noch andere Vereinbarungen, die für
einzelne Arbeitsgruppen relevant sind. Im Falle von Beobachtungsdaten,
die für CMEMS [2]_ vorgesehen sind, müssen z.B. die Regeln der
OceanSITES Format Referenz [3]_ eingehalten werden, die aber nicht
intern am Zentrum verwendet werden. Des Weiteren sind für andere Nutzer
eventuell die SeaDataNet netCDF-Konventionen von Relevanz [4]_, die
selbst auf den CF Konventionen beruhen.

Sind die Daten den aufgeführten Regeln entsprechend gespeichert, sollten
die Dateien mit dem 'CF-Convention Compliance Checker for NetCDF
Format' [5]_ geprüft werden. Zusätzlich sollte eine zweite Person
prüfen, ob die speziellen Hereon-Richtlinien befolgt wurden.

Nach erfolgter positiver Prüfung können Messdaten im COSYNA-Datenbereich
(Ansprechpartner: Gisbert Breitbach) und Modell-Daten z.B. in der
CERA-Datenbank des DKRZ mit doi-Vergabe gespeichert werden.

.. [1] http://cfconventions.org/index.html

.. [2] http://marine.copernicus.eu

.. [3] http://www.oceansites.org/docs/oceansites_data_format_reference_manual.pdf

.. [4] https://www.seadatanet.org/content/download/637/3338/file/SDN_D85_WP8_netCDF_files_examples.zip

.. [5] http://puma.nerc.ac.uk/cgi-bin/cf-checker.pl
